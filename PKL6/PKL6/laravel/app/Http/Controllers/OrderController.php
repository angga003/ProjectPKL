<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $data['module']['name'] = "Order";
        return view('adminlte.order.index',['data' => $data]);
    }
}
