<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function index()
    {
        $data['module']['name'] = "Package";
        return view('adminlte.package.index',['data' => $data]);
    }
}
