<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function index()
    {
        $data['module']['name'] = "Service";
        return view('adminlte.service.index',['data' => $data]);
    }
}
