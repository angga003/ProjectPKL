<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $data['module']['name'] = "Partner";
        return view('adminlte.users.index',['data' => $data]);
    }
   
}
