<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $data['module']['name'] = "Setting";
        return view('adminlte.setting.index',['data' => $data]);
    }
}
