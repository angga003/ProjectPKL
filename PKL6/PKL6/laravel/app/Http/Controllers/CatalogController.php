<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function index()
    {
        $data['module']['name'] = "Catalog";
        return view('adminlte.catalog.index',['data' => $data]);
    }
}
