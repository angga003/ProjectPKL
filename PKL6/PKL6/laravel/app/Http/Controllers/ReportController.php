<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index()
    {
        $data['module']['name'] = "Report";
        return view('adminlte.report.index',['data' => $data]);
    }
}
