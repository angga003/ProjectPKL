<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/student/create', 'StudentController@create')->name('student.create')->middleware('login_auth');

Route::post('/student', 'StudentController@store')->name('student.store')->middleware('login_auth');

Route::get('/student', 'StudentController@index')->name('student.index')->middleware('login_auth');

Route::get('/student/{student}', 'StudentController@show')->name('student.show')->middleware('login_auth');

Route::get('/student/{student}/edit', 'StudentController@edit')->name('student.edit')->middleware('login_auth');

Route::patch('/student/{student}', 'StudentController@update')->name('student.update')->middleware('login_auth');

Route::delete('/student/{student}', 'StudentController@destroy')->name('student.destroy')->middleware('login_auth');

Route::get('/adminlte/login', 'AdminController@index')->name('login.index');
Route::get('/adminlte/logout', 'AdminController@logout')->name('login.logout');
Route::post('/adminlte/login', 'AdminController@process')->name('login.process');

Route::get('/adminlte/index', 'AdminLTEController@index')->name('adminlte.index')->middleware('login_auth');

Route::get('/adminlte/student/create', 'AdminLTEStudentController@create')->name('adminlte.student.create')->middleware('login_auth');
Route::get('/adminlte/users/index', 'UserController@index')->name('adminlte.users.index')->middleware('login_auth');
Route::get('/adminlte/report/index', 'ReportController@index')->name('adminlte.report.index')->middleware('login_auth');
Route::get('/adminlte/order/index', 'OrderController@index')->name('adminlte.order.index')->middleware('login_auth');
Route::get('/adminlte/package/index', 'PackageController@index')->name('adminlte.package.index')->middleware('login_auth');
Route::get('/adminlte/catalog/index', 'CatalogController@index')->name('adminlte.catalog.index')->middleware('login_auth');
Route::get('/adminlte/service/index', 'ServiceController@index')->name('adminlte.service.index')->middleware('login_auth');
Route::get('/adminlte/setting/index', 'SettingController@index')->name('adminlte.setting.index')->middleware('login_auth');