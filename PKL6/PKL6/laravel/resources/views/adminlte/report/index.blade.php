@extends('admin_layout.app')
@section('header')
 @include('admin_layout.header')
@endsection
@section('leftbar')
 @include('admin_layout.leftbar')
@endsection
@section('rightbar')
 @include('admin_layout.rightbar')
@endsection
@section('content')
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>
 {{$data['module']['name']}}
 </h1>
 </section>
 <!-- Main content -->
 <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Report
            <small class="pull-right">Date: 2/10/2014</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          Role
          <address>
            <select class="selectpicker col-sm-12 pl-0 pr-0" data-style="select-with-transition" title="" data-size="100">
                <option value="1">{{ __('All Partner') }}</option>
                <option value="2">{{ __('...........') }}</option>
                <option value="3">{{ __('...........') }}</option>
            </select>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Date
          <address>
            <input type="date">
            <button type="button" class="btn btn-success pull-right">Cari
          </address>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Order Id</th>
              <th>Date</th>
              <th>Order Type</th>
              <th>Payment Method</th>
              <th>Customer</th>
              <th>Partner</th>
              <th>Store</th>
              <th>Chasier</th>
              <th>Total</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>
                <button type="button" class="btn btn-success">Edit
              </td>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      
    </section>
 <!-- /.content -->
 </div>
@endsection