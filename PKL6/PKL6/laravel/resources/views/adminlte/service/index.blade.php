@extends('admin_layout.app')
@section('header')
 @include('admin_layout.header')
@endsection
@section('leftbar')
 @include('admin_layout.leftbar')
@endsection
@section('rightbar')
 @include('admin_layout.rightbar')
@endsection
@section('content')
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>
 {{$data['module']['name']}}
 </h1>
 </section>
 <!-- Main content -->
 <section class="invoice">
      <!-- title row -->
      <div class="row">
      <div class="w3-bar w3-black">
              <button class="btn btn-sm btn-gray" onclick="openCity('London')">Whatsapp</button>
              <button class="btn btn-sm btn-gray" onclick="openCity('Paris')">Email</button>
              <button class="btn btn-sm btn-gray" onclick="openCity('Tokyo')">Push Notificastion</button>
            </div><br>
        <div class="col-xs-12">
        <div id="London" class="city">
              <TABLE style="text-align: left;" width="50%">
                <TR height="50">
                    <TH>
                      <label>All Partner:</label>
                    </TH>
                    <TH>
                      <select class="selectpicker col-sm-12 pl-0 pr-0" data-style="select-with-transition" title="" data-size="100">
                        <option value="1">{{ __('Random Sender') }}</option>
                        <option value="2">{{ __('........') }}</option>
                        <option value="3">{{ __('........') }}</option>
                      </select>
                    </TH>
                  </TR>
                  <TR height="50">
                    <TH>
                      <label>File Excel :</label>
                    </TH>
                    <TH>
                    <div>
                        <span class="btn btn-success btn-file">
                          <span class="fileinput-new">{{ __('Select file') }}</span>
                          <span class="fileinput-exists">{{ __('Change') }}</span>
                          <input type="file" name="photo" id = "input-picture" />
                        </span>
                    </div>
                    </TH>
                  </TR>
              </TABLE><br>
            </div>
            <div id="Paris" class="city" style="display:none">
              <h2>Email</h2>
              <p>........................</p>
            </div>

            <div id="Tokyo" class="city" style="display:none">
              <h2>Push Notification</h2>
              <p>.........................</p>
            </div><br>
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Service
            <small class="pull-right">Date: 2/10/2014</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          Role
          <address>
            <select class="selectpicker col-sm-12 pl-0 pr-0" data-style="select-with-transition" title="" data-size="100">
                <option value="1">{{ __('All Partner') }}</option>
                <option value="2">{{ __('...........') }}</option>
                <option value="3">{{ __('...........') }}</option>
            </select>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Date
          <address>
            <input type="date">
            <button type="button" class="btn btn-success pull-right">Cari
          </address>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Order Id</th>
              <th>Date</th>
              <th>Order Type</th>
              <th>Payment Method</th>
              <th>Customer</th>
              <th>Partner</th>
              <th>Store</th>
              <th>Total</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><button type="button" class="btn btn-success">Edit</td>
            </tr>
            <tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      
    </section>
 <!-- /.content -->
 </div>
 <script>
    function openCity(cityName) {
      var i;
      var x = document.getElementsByClassName("city");
      for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";  
      }
    document.getElementById(cityName).style.display = "block";  
    }
</script>
@endsection