@extends('layouts.app', ['activePage' => 'service-management', 'menuParent' => 'laravel', 'titlePage' => __('Service')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="w3-bar w3-black">
              <button class="btn btn-sm btn-gray" onclick="openCity('London')">Whatsapp</button>
              <button class="btn btn-sm btn-gray" onclick="openCity('Paris')">Email</button>
              <button class="btn btn-sm btn-gray" onclick="openCity('Tokyo')">Push Notificastion</button>
            </div><br>
            <div id="London" class="city">
              <TABLE style="text-align: left;" width="50%">
                <TR height="50">
                    <TH>
                      <label>All Partner:</label>
                    </TH>
                    <TH>
                      <select class="selectpicker col-sm-12 pl-0 pr-0" data-style="select-with-transition" title="" data-size="100">
                        <option value="1">{{ __('Random Sender') }}</option>
                        <option value="2">{{ __('........') }}</option>
                        <option value="3">{{ __('........') }}</option>
                      </select>
                    </TH>
                  </TR>
                  <TR height="50">
                    <TH>
                      <label>File Excel :</label>
                    </TH>
                    <TH>
                    <div>
                        <span class="btn btn-rose btn-file">
                          <span class="fileinput-new">{{ __('Select file') }}</span>
                          <span class="fileinput-exists">{{ __('Change') }}</span>
                          <input type="file" name="photo" id = "input-picture" />
                        </span>
                    </div>
                    </TH>
                  </TR>
              </TABLE><br>
            </div>

            <div id="Paris" class="city" style="display:none">
              <h2>Email</h2>
              <p>........................</p>
            </div>

            <div id="Tokyo" class="city" style="display:none">
              <h2>Push Notification</h2>
              <p>.........................</p>
            </div><br>
              <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">label</i>
                </div>
                <h4 class="card-title">{{ __('Service') }}</h4>
              </div>
              <div class="card-body">
                <TABLE style="text-align: center;" width="100%">
                  <TR height="50">
                    <TH>
                      <select class="selectpicker col-sm-12 pl-0 pr-0" data-style="select-with-transition" title="" data-size="100">
                        <option value="1">{{ __('Admin') }}</option>
                        <option value="2">{{ __('Creator') }}</option>
                        <option value="3">{{ __('Member') }}</option>
                      </select>
                    </TH>
                    <TH>
                      <input type="date" title="">
                      <a href="{{ route('service.create') }}" class="btn btn-sm btn-rose">{{ __('Filter Date') }}</a>
                    </TH>
                  </TR>
                </TABLE>
                @can('create', App\Service::class)
                  <div class="row">
                    <div class="col-12 text-right">
                      <a href="{{ route('service.create') }}" class="btn btn-sm btn-rose">{{ __('Filter Date') }}</a>
                    </div>
                  </div>
                @endcan
                <div class="table-responsive">
                  <table id="datatables" class="table table-striped table-no-bordered table-hover" style="display:none">
                    <thead class="text-primary">
                      <th>
                          {{ __('Order Id') }}
                      </th>
                      <th>
                        {{ __('Date') }}
                      </th>
                      <th>
                        {{ __('Order type') }}
                      </th>
                      <th>
                        {{ __('Payment Method') }}
                      </th>
                      <th>
                        {{ __('Customer') }}
                      </th>
                      <th>
                        {{ __('Partner') }}
                      </th>
                      <th>
                        {{ __('Store') }}
                      </th>
                      <th>
                        {{ __('Chasier') }}
                      </th>
                      <th>
                        {{ __('Total') }}
                      </th>
                      <th>
                        {{ __('Status') }}
                      </th>
                      @can('manage-items', App\User::class)
                        <th class="text-right">
                          {{ __('Actions') }}
                        </th>
                      @endcan
                    </thead>
                    <tbody>
                      @foreach($services as $service)
                        <tr>
                          <td>
                            
                          </td>
                          <td>

                          </td>
                          <td>

                          </td>
                          
                          <td>
                            
                          </td>
                          
                          <td>
                            
                          </td>
                          <td>
                            
                          </td>
                          <td>
                            
                          </td>
                          <td>
                            
                          </td>
                          <td>
                            
                          </td>
                          <td>
                            
                          </td>
                          @can('manage-items', App\User::class)
                            <td class="td-actions text-right">
                              <form action="{{ route('service.destroy', $service) }}" method="post">
                                @csrf
                                @method('delete')
                                
                                @can('update', $service)
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('service.edit', $service) }}" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                @endcan
                                @if ($service->items->isEmpty() && auth()->user()->can('delete', $service))
                                  <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this service?") }}') ? this.parentElement.submit() : ''">
                                      <i class="material-icons">close</i>
                                      <div class="ripple-container"></div>
                                  </button>
                                @endif
                              </form>
                            </td>
                          @endcan
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
<script>
  $(document).ready(function() {
    $('#datatables').fadeIn(1100);
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search services",
      },
      "columnDefs": [
        { "orderable": false, "targets": 3 },
      ],
    });
  });
</script>
  <script>
    function openCity(cityName) {
      var i;
      var x = document.getElementsByClassName("city");
      for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";  
      }
    document.getElementById(cityName).style.display = "block";  
    }
</script>
@endpush