@extends('layouts.app', ['activePage' => 'tag-management', 'menuParent' => 'laravel', 'titlePage' => __('Report')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="w3-bar w3-black">
              <button class="btn btn-sm btn-gray" onclick="openCity('London')">Order</button>
              <button class="btn btn-sm btn-gray" onclick="openCity('Paris')">Lost Card</button>
              <button class="btn btn-sm btn-gray" onclick="openCity('Tokyo')">Payment Package</button>
              <button class="btn btn-sm btn-gray" onclick="openCity('Purwokerto')">KYC</button>
            </div><br>
            <div id="London" class="city">
            <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">label</i>
                </div>
                <h4 class="card-title">{{ __('Report') }}</h4>
              </div>
              <div class="card-body">
                <TABLE style="text-align: center;" width="100%">
                  <TR height="50">
                    <TH>
                      <select class="selectpicker col-sm-12 pl-0 pr-0" data-style="select-with-transition" title="" data-size="100">
                        <option value="1">{{ __('All Partner') }}</option>
                        <option value="2">{{ __('...........') }}</option>
                        <option value="3">{{ __('...........') }}</option>
                      </select>
                    </TH>
                    <TH>
                      <input type="date" title="">
                      <a href="{{ route('tag.create') }}" class="btn btn-sm btn-rose">{{ __('Filter Date') }}</a>
                    </TH>
                  </TR>
                </TABLE>
                @can('create', App\Tag::class)
                  <div class="row">
                    <div class="col-12 text-right">
                      <a href="{{ route('tag.create') }}" class="btn btn-sm btn-rose">{{ __('Filter Date') }}</a>
                    </div>
                  </div>
                @endcan
                <div class="table-responsive">
                  <table id="datatables" class="table table-striped table-no-bordered table-hover" style="display:none">
                    <thead class="text-primary">
                    <th>
                          {{ __('Order Id') }}
                      </th>
                      <th>
                        {{ __('Date') }}
                      </th>
                      <th>
                        {{ __('Order type') }}
                      </th>
                      <th>
                        {{ __('Payment Method') }}
                      </th>
                      <th>
                        {{ __('Customer') }}
                      </th>
                      <th>
                        {{ __('Partner') }}
                      </th>
                      <th>
                        {{ __('Store') }}
                      </th>
                      <th>
                        {{ __('Chasier') }}
                      </th>
                      <th>
                        {{ __('Total') }}
                      </th>
                      <th>
                        {{ __('Status') }}
                      </th>
                      @can('manage-items', App\User::class)
                        <th class="text-right">
                          {{ __('Actions') }}
                        </th>
                      @endcan
                    </thead>
                    <tbody>
                      @foreach($tags as $tag)
                        <tr>
                        <td>
                            
                            </td>
                            <td>
  
                            </td>
                            <td>
  
                            </td>
                            
                            <td>
                              
                            </td>
                            
                            <td>
                              
                            </td>
                            <td>
                              
                            </td>
                            <td>
                              
                            </td>
                            <td>
                              
                            </td>
                            <td>
                              
                            </td>
                            <td>
                              
                            </td>
                          @can('manage-items', App\User::class)
                            <td class="td-actions text-right">
                              <form action="{{ route('tag.destroy', $tag) }}" method="post">
                                @csrf
                                @method('delete')
                                
                                @can('update', $tag)
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('tag.edit', $tag) }}" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                @endcan
                                @if ($tag->items->isEmpty() && auth()->user()->can('delete', $tag))
                                  <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this tag?") }}') ? this.parentElement.submit() : ''">
                                      <i class="material-icons">close</i>
                                      <div class="ripple-container"></div>
                                  </button>
                                @endif
                              </form>
                            </td>
                          @endcan
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div id="Paris" class="city" style="display:none">
              <h2>Email</h2>
              <p>........................</p>
            </div>

            <div id="Tokyo" class="city" style="display:none">
              <h2>Push Notification</h2>
              <p>.........................</p>
            </div>
            <div id="Purwokerto" class="city" style="display:none">
              <h2>Email</h2>
              <p>........................</p>
            </div><br>
              
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
<script>
  $(document).ready(function() {
    $('#datatables').fadeIn(1100);
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search tags",
      },
      "columnDefs": [
        { "orderable": false, "targets": 3 },
      ],
    });
  });
</script>
  <script>
    function openCity(cityName) {
      var i;
      var x = document.getElementsByClassName("city");
      for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";  
      }
    document.getElementById(cityName).style.display = "block";  
    }
</script>
@endpush