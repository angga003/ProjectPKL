<?php
/*

=========================================================
* Argon Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-laravel
* Copyright 2018 Creative Tim (https://www.creative-tim.com) & UPDIVISION (https://www.updivision.com)

* Coded by www.creative-tim.com & www.updivision.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
namespace App\Http\Controllers;

use App\Setting;
use App\User;
use App\Http\Requests\SettingRequest;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Setting::class);
    }

    /**
     * Display a listing of the tags
     *
     * @param \App\Setting  $model
     * @return \Illuminate\View\View
     */
    public function index(Setting $model)
    {
        $this->authorize('manage-items', User::class);

        return view('settings.index', ['settings' => $model->all()]);
    }

    /**
     * Show the form for creating a new tag
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('settings.create');
    }

    /**
     * Store a newly created tag in storage
     *
     * @param  \App\Http\Requests\SettingRequest  $request
     * @param  \App\Setting  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SettingRequest $request, Setting $model)
    {
        $model->create($request->all());

        return redirect()->route('setting.index')->withStatus(__('Setting successfully created.'));
    }

    /**
     * Show the form for editing the specified tag
     *
     * @param  \App\Setting  $tag
     * @return \Illuminate\View\View
     */
    public function edit(Setting  $setting)
    {
        return view('settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SettingRequest  $request
     * @param  \App\Setting  $tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SettingRequest $request, Setting $setting)
    {
        $setting->update($request->all());

        return redirect()->route('setting.index')->withStatus(__('Service successfully updated.'));
    }

    /**
     * Remove the specified tag from storage
     *
     * @param  \App\Setting  $tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Setting $setting)
    {
        if (!$setting->items->isEmpty()) {
            return redirect()->route('setting.index')->withErrors(__('This tag has items attached and can\'t be deleted.'));
        }

        $setting->delete();

        return redirect()->route('setting.index')->withStatus(__('Service successfully deleted.'));
    }
}
