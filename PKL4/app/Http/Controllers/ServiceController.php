<?php
/*

=========================================================
* Argon Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-laravel
* Copyright 2018 Creative Tim (https://www.creative-tim.com) & UPDIVISION (https://www.updivision.com)

* Coded by www.creative-tim.com & www.updivision.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
namespace App\Http\Controllers;

use App\Service;
use App\User;
use App\Http\Requests\ServiceRequest;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Service::class);
    }

    /**
     * Display a listing of the tags
     *
     * @param \App\Service  $model
     * @return \Illuminate\View\View
     */
    public function index(Service $model)
    {
        $this->authorize('manage-items', User::class);

        return view('services.index', ['services' => $model->all()]);
    }

    /**
     * Show the form for creating a new tag
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('services.create');
    }

    /**
     * Store a newly created tag in storage
     *
     * @param  \App\Http\Requests\ServiceRequest  $request
     * @param  \App\Service  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ServiceRequest $request, Service $model)
    {
        $model->create($request->all());

        return redirect()->route('service.index')->withStatus(__('Service successfully created.'));
    }

    /**
     * Show the form for editing the specified tag
     *
     * @param  \App\Service  $tag
     * @return \Illuminate\View\View
     */
    public function edit(Service  $service)
    {
        return view('services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ServiceRequest  $request
     * @param  \App\Service  $tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ServiceRequest $request, Service $service)
    {
        $service->update($request->all());

        return redirect()->route('service.index')->withStatus(__('Service successfully updated.'));
    }

    /**
     * Remove the specified tag from storage
     *
     * @param  \App\Service  $tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Service $service)
    {
        if (!$service->items->isEmpty()) {
            return redirect()->route('service.index')->withErrors(__('This tag has items attached and can\'t be deleted.'));
        }

        $service->delete();

        return redirect()->route('service.index')->withStatus(__('Service successfully deleted.'));
    }
}
