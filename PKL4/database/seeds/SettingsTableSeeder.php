<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('settings')->insert([
            'id' => 1,
            'name' => 'Hot',
            'color' => '#f44336',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('settings')->insert([
            'id' => 2,
            'name' => 'Trending',
            'color' => '#9c27b0',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('settings')->insert([
            'id' => 3,
            'name' => 'New',
            'color' => '#00bcd4',
            'created_at' => now(),
            'updated_at' => now()
        ]);

    }
}
